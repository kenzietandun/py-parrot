#!/usr/bin/env python3

#---------------------------
# Written by Kenzie Tandun
# Dec 07, 2018
# nudnateiznek@gmail.com
#---------------------------

import subprocess
import sys
import configparser

def p_error(string):
    """ prints error message then exits """
    for s in string.split('\n'):
        print("[Error] {}".format(s.lstrip()))
    print("[Error] Exiting...")
    sys.exit()

class Config:
    def __init__(self, config_file):
        """ configuration class initialiser 
        specify config_file with location of configuration file """
        self.monitor_dict = {}
        self.config_file = config_file
        self.monitor_placement = {}
        self.monitors = 0
        self.verify_config()
        self.parse_monitor_config()
        print("[OK] configuration is a-o-kay :)")

    def verify_config(self):
        """ verifies config file specified is valid:
        - checks if monitor_num is filled with integer
        - checks if there are correct number of monitor sections
        """

        self.config = configparser.ConfigParser()
        self.config.read(self.config_file)
        monitor_num = self.config.getint("general", "monitor_num")
        if not monitor_num:
            p_error("monitor_num in general section must be filled.")

        if int(monitor_num) <= 0:
            p_error("monitor_num in general section must be greater than 0.")

        config_sections = self.config.sections()
        self.monitors = len(config_sections) - 1
        if self.monitors != int(monitor_num):
            p_error("number of monitors in config file is not equal to monitor_num specified.")

        self.monitor_names = []

        for i in range(1, self.monitors + 1):
            self.monitor_names.append("mon{}".format(i))

        #self.verify_monitor_valid_names()
        self.verify_monitor_placement()
        self.verify_led_nums()

    def verify_monitor_valid_names(self):
        """ verifies if the monitor names specified in config
        file matches with the one from xrandr """
        cmd = "xrandr | grep connected | grep -v disconnected | awk '{print $1}'"
        running_monitors = subprocess.getoutput(cmd)
        running_monitors = running_monitors.split('\n')
        
        for monitor in self.monitor_names:
            monitor_name = self.config.get(monitor, "name")
            if monitor_name in running_monitors:
                running_monitors.remove(monitor_name)
            else:
                p_error("Invalid monitor name {}".format(monitor_name))

    def verify_monitor_placement(self):
        """ verifies the monitors specified in config file are
        correctly configured 
        e.g.
        if mon1 specifies that mon2 is on its right then
        mon2 should have mon1 on its left """
        orientations = ["left", "right", "bottom", "top"]
        orientation_opposite = {"left": "right", "top": "bottom",
                                "right": "left", "bottom": "top"}

        for monitor in self.monitor_names:
            for orientation in orientations:
                adjacent_monitor = self.config.get(monitor, orientation)
                if adjacent_monitor:
                    try:
                        adjacent_monitor_opposite = self.config.get(adjacent_monitor, 
                                orientation_opposite[orientation])
                    except configparser.NoSectionError:
                        p_error("""There is no section for monitor called {} in the config file""".format(adjacent_monitor))
                    if adjacent_monitor_opposite != monitor:
                        p_error("Monitor {adj} should have {mon} on its {ori} in the config file and vice versa".format(
                            adj=adjacent_monitor, mon=monitor, ori=orientation_opposite[orientation]))

    def verify_led_nums(self):
        """ verifies that the led nums needed are 
        filled in the config file
        e.g.
        if mon1 is to the left of mon2 then mon1 should have 
        leds on its top, left and bottom """
        led_placement = {"left": "num_leds_left", 
                         "right": "num_leds_right", 
                         "top": "num_leds_top", 
                         "bottom": "num_leds_bottom"}

        for monitor in self.monitor_names:
            places_to_check = list(led_placement.values())
            orientations = list(led_placement.keys())
            for orientation in orientations:
                adjacent_monitor = self.config.get(monitor, orientation)
                if adjacent_monitor:
                    places_to_check.remove(led_placement[orientation])

            for place in places_to_check:
                num_leds = self.config.get(monitor, place)
                if not num_leds:
                   p_error("{loc} must not be empty for {mon}".format(
                        loc=place, mon=monitor))

    def parse_monitor_config(self):
        """ reads from the config file and 
        creates a dictionary of monitors and its configurations 
        for example:

        {'mon1': {'right': 'mon2', 
                  'num_leds_top': '18', 
                  'num_leds_left': '10', 
                  'num_leds_bottom': '18'}, 
         'mon2': {'left': 'mon1', 
                  'right': 'mon3', 
                  'num_leds_top': '20', 
                  'num_leds_bottom': '20'}
                  }
        """
        monitors_config = {}
        for monitor in self.monitor_names:
            monitor_config = {}
            for field in self.config[monitor]:
                value = self.config.get(monitor, field)
                if value:
                    monitor_config[field] = value

            monitors_config[monitor] = monitor_config

        self.monitor_dict = monitors_config

    def get_monitor_dict(self):
        """
        Returns monitor_dict that has been parsed by 
        parse_monitor_config()
        """

        return self.monitor_dict

        

    def get_monitor_arrangement(self):
        """ given a dictionary of monitor configs, 
        calculate the location of each monitor and return
        a list of (monitor_name, num_of_leds) sorted
        in a clockwise fashion starting from the leftmost monitor

        for example:
        In an arrangement of mon1 -- mon2 -- mon3

        this function should return:

        [('mon1', 'bottom'), ('mon1', 'left'), 
        ('mon1', 'top'), ('mon2', 'top'), ('mon3', 'top'), 
        ('mon3', 'right'), ('mon3', 'bottom'), ('mon2', 'bottom')]

        first it traverses through the first monitor's top side, 
        then second monitor's top side, 
        third monitor's top side,
        thrid monitor's right side, 
        third monitor's bottom side, 
        second monitor's bottom side,
        first monitor's bottom side,
        first monitor's left side,
        the lastly first monitors' left side.
        """

        arrangement = []

        start_mon = self.config.get("general", "start_monitor")
        start_side = self.config.get("general", "start_side")

        ordered_monitors = self.order_monitors("mon1")
        sides = ["top", "bottom", "right", "left"] 
        total_sides = 4
        for monitor in ordered_monitors:
            filled_sides = []
            for side in sides:
                if side in self.monitor_dict[monitor].keys():
                    filled_sides.append(side)
            for side in self.find_empty_sides(monitor, filled_sides):
                led_num_str = "num_leds_{}".format(side)
                arrangement.append((monitor, side, self.monitor_dict[monitor][led_num_str]))

        start_mon_index = arrangement.index((start_mon, 
            start_side, self.monitor_dict[start_mon][led_num_str]))

        first_half = arrangement[start_mon_index:]
        second_half = arrangement[:start_mon_index]
        first_half.extend(second_half)
        return first_half 

    def find_empty_sides(self, monitor, filled_sides):
        """
        given a monitor and its sides that are next to another
        monitors, return its empty sides in clockwise order
        """

        monitor_sides = 4
        empty_sides_count = monitor_sides - len(filled_sides)
        side_priority = ["top", "right", "bottom", "left"]

        if empty_sides_count == 0:
            return []

        elif empty_sides_count == 1:
            for side in filled_sides:
                side_priority.remove(side)

            return side_priority

        elif empty_sides_count == 2:
            for side in side_priority:
                if side not in filled_sides:
                    side_priority.remove(side)

                    self.monitor_dict[monitor][side] = "LED"
                    return [side]

        elif empty_sides_count == 3:
            side_priority.extend(side_priority)
            filled_index = side_priority.index(filled_sides[0])

            return side_priority[filled_index+1:filled_index+1+empty_sides_count]

        elif empty_sides_count == 4:
            return side_priority

    def order_monitors(self, current_monitor, monitors=[], mode="forward"):
        """ given a name of a monitor, get the next monitor
        according to ordering in get_monitor_arrangement() 
        Returns a List of monitors"""

        if monitors and current_monitor == monitors[0]:
            final_list = []
            
            for i in range(0, len(monitors)):
                if final_list and monitors[i] != final_list[-1]:
                    final_list.append(monitors[i])

                if not final_list:
                    final_list.append(monitors[i])

            return final_list

        monitors.append(current_monitor)

        if mode == "forward":

            if "right" in self.monitor_dict[current_monitor]:
                curr_right = self.monitor_dict[current_monitor]["right"]
                if curr_right in monitors:
                    return self.order_monitors(current_monitor, monitors=monitors,
                            mode="reverse")
                if "top" in self.monitor_dict[curr_right]:
                    next_monitor = self.monitor_dict[curr_right]["top"]
                else:
                    next_monitor = curr_right
                return self.order_monitors(next_monitor, monitors=monitors)

            elif "bottom" in self.monitor_dict[current_monitor]:
                curr_bot = self.monitor_dict[current_monitor]["bottom"]
                if "right" in self.monitor_dict[curr_bot]:
                    next_monitor = self.monitor_dict[curr_bot]["right"]
                else:
                    next_monitor = curr_bot
                return self.order_monitors(next_monitor) 

            elif "top" in self.monitor_dict[current_monitor]:
                curr_top = self.monitor_dict[current_monitor]["top"]
                next_monitor = curr_top
                if next_monitor in monitors:
                    return self.order_monitors(current_monitor, mode="reverse")
                else:
                    return self.order_monitors(next_monitor)

            elif "left" in self.monitor_dict[current_monitor]:
                curr_left = self.monitor_dict[current_monitor]["left"]
                if "bottom" in self.monitor_dict[curr_left]:
                    next_monitor = self.monitor_dict[curr_left]["bottom"]
                    return self.order_monitors(next_monitor)
                else:
                    next_monitor = curr_left
                    return self.order_monitors(next_monitor, mode="reverse")

            else:
                return [current_monitor]

        elif mode == "reverse":

            if "left" in self.monitor_dict[current_monitor]:
                curr_left = self.monitor_dict[current_monitor]["left"]
                if "bottom" in self.monitor_dict[curr_left]:
                    next_monitor = self.monitor_dict[curr_left]["bottom"]
                    return self.order_monitors(next_monitor, mode="forward")
                else:
                    next_monitor = curr_left
                return self.order_monitors(next_monitor, mode=mode)

            elif "top" in self.monitor_dict[current_monitor]:
                curr_top = self.monitor_dict[current_monitor]["top"]
                if "left" in self.monitor_dict[curr_top]:
                    next_monitor = self.monitor_dict[curr_top]["left"]
                else:
                    next_monitor = curr_top
                return self.order_monitors(next_monitor, mode=mode)

if __name__ == "__main__":
    c = Config("./config.ini")
    print(c.get_monitor_arrangement())
