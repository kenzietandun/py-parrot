#!/usr/bin/env python3

#---------------------------
# Author: Kenzie Tandun
# Dec 16, 2018
# nudnateiznek@gmail.com
#---------------------------

import screen
import configuration

config_file = "./config.ini"

def main():
    cfg = configuration.Config(config_file)
    monitor_arr = cfg.get_monitor_arrangement()
    monitor_dict = cfg.get_monitor_dict()
    print(monitor_arr)

    pi_addr = "http://10.10.1.2:5000"
    screen.get_screen_colors_and_send_to_pi(monitor_arr, monitor_dict, pi_addr, skip_leds=2)

if __name__ == "__main__":
    main()


