#!/usr/bin/env python3

#---------------------------
# Author: Kenzie Tandun
#
# nudnateiznek@gmail.com
#---------------------------

from PIL import Image
import time
import mss
import mss.tools
import subprocess
import re
import requests

displacement = 1
height = 1
increment = 100

def get_color(sct, location):
    sct_img = sct.grab(location)
    img = Image.frombytes("RGB", sct_img.size, sct_img.bgra, "raw", "BGRX").resize((1, 1))
    r, g, b = img.getpixel((0, 0))

    return (r, g, b)

def screen_colors_mon(side, monitor_number, 
        mon_top, mon_left, mon_height, mon_width, led_nums, sct):
    colors = []
    if side == "top":
        distance_between_leds = mon_width // int(led_nums)
        for x in range(led_nums):
            location = {"top": mon_top + displacement, 
                        "left": mon_left + (x * distance_between_leds), 
                        "width": distance_between_leds,
                        "height": height,
                        "mon": monitor_number
                       }
            colors.append(get_color(sct, location))

    elif side == "right":
        distance_between_leds = mon_height // int(led_nums)
        for y in range(led_nums):
            location = {"top": mon_top + y * distance_between_leds,
                        "left": mon_left + mon_width - displacement - height - 1,
                        "width": height, 
                        "height": distance_between_leds,
                        "mon": monitor_number
                       }
            colors.append(get_color(sct, location))

    elif side == "bottom":
        distance_between_leds = mon_width // int(led_nums)
        for x in range(led_nums, 0, -1):
            location = {"top": mon_top + mon_height - height - displacement - 1, 
                        "left": mon_left + (x - 1) * distance_between_leds, 
                        "width": distance_between_leds,
                        "height": height,
                        "mon": monitor_number
                       }
            colors.append(get_color(sct, location))

    elif side == "left":
        distance_between_leds = mon_height // int(led_nums)
        for y in range(led_nums, 0, -1):
            location = {"top": mon_top + (y - 1) * distance_between_leds,
                        "left": mon_left + displacement,
                        "width": height, 
                        "height": distance_between_leds,
                        "mon": monitor_number
                       }
            colors.append(get_color(sct, location))

    return colors

def get_screen_colors_and_send_to_pi(monitor_arrangement, monitor_dict, 
        pi_addr, skip_leds=0):

    monitor_map = map_screens_to_mss()
    prev_colors = []
    count = 0

    with mss.mss() as sct:
        while True:
            colors = []
            for i in range(skip_leds):
                colors.append((0, 0, 0))

            for monitor, side, led_nums in monitor_arrangement:

                monitor_number = monitor_map[monitor_dict[monitor]["name"]]
                led_nums = int(led_nums)

                mon = sct.monitors[monitor_number]
                mon_top = mon["top"]
                mon_left = mon["left"]
                mon_height = mon["height"]
                mon_width = mon["width"]
                colors.extend(screen_colors_mon(side, monitor_number, 
                        mon_top, mon_left,
                        mon_height, mon_width, led_nums, sct))

            if count == 0:
                send_colors_to_pi(colors, pi_addr)
                count = 1
            else:
                colors_diff = get_colors_diff(colors, prev_colors)
                if colors_diff:
                    send_colors_diff_to_pi(colors_diff, pi_addr)

            prev_colors = colors
            time.sleep(0.25)
            
            count += 1

def get_colors_diff(colors, prev_colors):
    colors_diff = []
    for index, color in enumerate(colors):
        if colors[index] != prev_colors[index]:
            colors_diff.append((index, colors[index]))
    return colors_diff

def send_colors_to_pi(colors, pi_addr):
    colors_str = ""
    for r, g, b in colors:
        colors_str += "{},{},{}+".format(r, g, b)
    r = requests.post("{}/api/{}".format(pi_addr, colors_str))

def send_colors_diff_to_pi(colors, pi_addr):
    colors_str = ""
    for led_num, color in colors:
        r, g, b = color
        colors_str += "{},{},{},{}+".format(led_num, r, g, b)
    r = requests.post("{}/diff/{}".format(pi_addr, colors_str))

def map_screens_to_mss():
    """
    maps current screens to mss' sct.monitors
    Returns a dict of monitor_name -> screen number according to mss

    mss uses xrandr as the backend so we'll sort it according to xrandr as well
    """

    output = subprocess.run("xrandr | grep connected | grep -v disconnected", 
            shell=True, stdout=subprocess.PIPE, universal_newlines=True)

    matches = re.findall("([\w\-]+)\sconnected\s", 
            output.stdout, re.MULTILINE)

    screen_map = {}
    mon_num = 1
    for monitor_name in matches:
        screen_map[monitor_name] = mon_num
        mon_num += 1

    return screen_map
