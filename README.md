<img src="https://gitlab.com/kenzietandun/py-parrot/raw/master/logo/ParrotGreen_withLine.png" height=150>

# py-parrot

is yet another script written in python3 to create something like Ambilight

### Installation

- create a virtualenv for python3 with `virtualenv -p python3 py-parrot`

- `cd py-parrot`

- clone this repository with `git clone https://gitlab.com/kenzietandun/py-pparrot src`

- activate the virtualenv `source bin/activate`

- install the requirements `pip install -r src/req.txt`

- `cd src`

### Usage

- edit `config.ini` according to your setup

- run `main.py`

### Todo

- code cleanup

- better README
