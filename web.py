#!/usr/bin/env python3

from flask import Flask

app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True

@app.route('/api/<colors>', methods=["POST"])
def set_led_color(colors):
    num = 0
    print(colors)
    return "OK"

@app.route('/api/all/<color>')
def set_all_color(color):
    print("Setting all LED to color {color}".format(color=color))
    return "OK"

if __name__ == "__main__":
    app.run(host="0.0.0.0")
